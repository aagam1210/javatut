package bank;

import java.util.regex.*;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.io.File;
import java.util.Scanner;

public class BankClerk {
    private Scanner commandScanner;
    private PrintWriter responseWriter;
    private bank bk;
    public BankClerk(bank b, InputStream in, OutputStream out) {
        this.bk = b;
        this.commandScanner = new Scanner(in);
        this.responseWriter = new PrintWriter(out, true);
    }
    public BankClerk(bank b) {
        this(b, System.in, System.out);
    }
    public enum Command {
        OPEN("[Oo][Pp][Ee][Nn]\\s+([SsCc])\\s+(\\w+)\\s+(\\d+)"),
        DEPOSIT("[Dd][Ee][Pp][Oo][Ss][Ii][Tt]\\s+(\\d+)\\s+(\\d+)"),
        WITHDRAW("[Ww][Ii][Tt][Hh][Dd][Rr][Aa][Ww]\\s+(\\d+)\\s+(\\d+)"),
        DISPLAY("[Dd][Ii][Ss][Pp][Ll][Aa][Yy]\\s+(\\d+)"),
        PASSBOOK("[Pp][Aa][Ss][Ss][Bb][Oo][Oo][Kk]\\s+(\\d+)"),
//        CLOSE("[Cc][Ll][Oo][Ss][Ee]\\s+(\\d+)"),
        LIST("[Ll][Ii][Ss][Tt]"),
        SAVE("[Ss][Aa][Vv][Ee]"),
        QUIT("[Qq][Uu][Ii][Tt]"),
        ;
        private Pattern syntax;
        Command(String regex) {
            this.syntax = Pattern.compile(regex);
        }
        public Pattern getSyntax() {
            return this.syntax;
        }
        public static Command getCommand(Matcher m) {
            Command[] commands = Command.values();
            for (Command command : commands) {
                if (m.usePattern(command.getSyntax()).matches()) {
                    return command;
                }
            }
            return null;
        }
    }
    public void run() {
        Matcher matcher = Pattern.compile("").matcher("");
        Command command = null;
        while(
            (command = 
                (Command.getCommand(
                    matcher.reset((commandScanner.nextLine())))
                )
            ) 
                != Command.QUIT) {
            if (command == null) {
                this.responseWriter.println("invalid command");
                continue;
            }
            try {
                switch (command) {
                    case OPEN:
                        char typeChar = matcher.group(1).charAt(0);
                        String name = matcher.group(2);
                        long amt = Long.parseLong(matcher.group(3));
                        bank.Type type = null;
                        switch(typeChar) {
                            case 'S':
                            case 's':
                                type = bank.Type.SAVINGS;
                                break;
                            case 'C':
                            case 'c':
                                type = bank.Type.CURRENT;
                        }
                        long acno = bk.openCurrentAccount(type, name, amt);
                        bk.display(acno);
                        continue;
                    case DEPOSIT:
                        acno = Long.parseLong(matcher.group(1));
                        amt = Long.parseLong(matcher.group(2));
                        bk.deposit(acno, amt);
                        bk.display(acno);
                        continue;
                    case WITHDRAW:
                        acno = Long.parseLong(matcher.group(1));
                        amt = Long.parseLong(matcher.group(2));
                        if (!bk.withdraw(acno, amt)) {
                            this.responseWriter.println("Warning:insufficient balance");
                        }
                        bk.display(acno);
                        continue;
                    case DISPLAY:
                        acno = Long.parseLong(matcher.group(1));
                        bk.display(acno);
                        continue;
                    case PASSBOOK:
                        acno = Long.parseLong(matcher.group(1));
                        bk.printpassbook(acno);
                        continue;
                   /* case CLOSE:
                        acno = Long.parseLong(matcher.group(1));
                        long closingBal = bk.closeAccount(acno);
                        this.responseWriter.println("Account "+acno+" closed with balance "+closingBal);
                        continue; */
                    case LIST:
                        bk.listAccounts();
                        continue;
                    case SAVE:
                        bk.save();
                        this.responseWriter.println("Saved successfully");
                        continue;
                    default:
                        this.responseWriter.println("Command not implemented");
                        continue;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
    public static void main(String[] args) throws Exception {
        String bankName = args[0];
        String bankFileName = bankName+".bank";
        bank bk = null;
        File bankFile = new File(bankFileName);
        if (bankFile.exists()) {
            bk = bk.load(bankName);
        } else {
            int bankCode = Integer.parseInt(args[1]);
            bk = new bank(bankName, bankCode);
        }
        BankClerk clerk = new BankClerk(bk);
        clerk.run();
    }
}

